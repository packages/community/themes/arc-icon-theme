# Maintainer: Stefano Capitani <stefanoatmanjarodotorg>
# Author: horst3180 @ deviantart
# This PKGBUILD provide both versions of Arc icon theme

pkgname=('arc-maia-icon-theme')
_pkgbase=arc-icon-theme
pkgver=20161122
pkgrel=4
arch=('any')
url="https://github.com/horst3180/arc-icon-theme"
license=('GPL3')
depends=('gtk-update-icon-cache' 'hicolor-icon-theme')
makedepends=('inkscape' 'optipng' 'libcanberra') 
optdepends=('elementary-icon-theme: Secondary icon theme fallback'
            'arc-themes-maia: gtk theme of arc-maia')
options=('!emptydirs' '!strip')
source=("$_pkgbase-$pkgver.tar.gz::https://github.com/horst3180/$_pkgbase/archive/$pkgver.tar.gz"
		'render_icons.sh')

sha512sums=('5d0f1417358eb81994868949acefe146537d8f3cc2fd7f529f9e6ba9c264845e50962f94427bac1262a76d3ca98d05795819d7c4a6ecd3139b0b57a6e9fdfad1'
            'a7695ce03ed2545ad5a0b1c5193d38a71ca6f51a6312a2c980751402d70d3492a41392e6d97351c345639353494a379609a1f9eed30bcc6793bfa79946a6aa07')

_prepare-arc-maia-icon-theme() {
# Rebuild the Arc icon Theme to provide the Maia version
	
	cd $srcdir	
    cd "$srcdir/$_pkgbase-$pkgver-maia"
    
    # Remove Arc folder ( this contain the arc icons )
    rm -Rf Arc
    
    # Change the Arc Blue color into any file in Maia variant
    find . -type f -name '*.*' -exec sed -i "s/6ba4e7/44E2BC/g" {}  \;
    find . -type f -name '*.*' -exec sed -i "s/5294e2/16A085/g" {}  \;
    find . -type f -name '*.*' -exec sed -i "s/64a0e6/44E2BC/g" {}  \;

    cd "$srcdir/$_pkgbase-$pkgver-maia/src"
     
    # Start icons rendering 
    echo
    msg "Create arc-icon-theme-maia:this next bit might take a little while..."
    echo
    sh ./render_icons.sh
     
    # Add moka, faba and elementary theme as fallback theme
    cd "$srcdir/$_pkgbase-$pkgver-maia"
    sed -i "s/Inherits=.*/Inherits=Moka,Faba,elementary,Adwaita,gnome,hicolor/" Arc/index.theme
    
    # Change Theme name in Arc Maia
    sed -i "s/Name=.*/Name=Arc-Maia/" Arc/index.theme
    sed -i "s/Comment=.*/Comment=Arc Maia Icon theme/" Arc/index.theme
}

prepare() {
cp -R "$srcdir/$_pkgbase-$pkgver" "$srcdir/$_pkgbase-$pkgver-maia"
cp -f render_icons.sh "$srcdir/$_pkgbase-$pkgver-maia/src/"

}

_build_arc-maia-icon-theme() {
	_prepare-arc-maia-icon-theme
    cd "$srcdir/$_pkgbase-$pkgver-maia"
    ./autogen.sh --prefix=/usr
    make
}

package_arc-maia-icon-theme() {
pkgdesc='Arc icon theme Manjaro variant.'
	_build_arc-maia-icon-theme
    cd "$srcdir/$_pkgbase-$pkgver-maia"
    make DESTDIR="${pkgdir}" install

    # Move the Theme into new folder Arc-Maia
    cd "$pkgdir/usr/share/icons"
    mv Arc Arc-Maia
}
